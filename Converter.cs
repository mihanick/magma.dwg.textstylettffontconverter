﻿using System;
using System.IO;
using System.Windows.Forms;
using HostMgd.ApplicationServices;
using HostMgd.EditorInput;
using Teigha.DatabaseServices;
using Teigha.GraphicsInterface;
using Teigha.Runtime;

namespace Magma.Dwg.TextStyleTtfFontConverter
{
	public class App : IExtensionApplication
	{
		public void Initialize()
		{

		}

		public void Terminate()
		{

		}
	}

	public class Converter
	{
		[CommandMethod("ConvertShxTtf", CommandFlags.Session)]
		public static void Convert()
		{
			// https://stackoverflow.com/questions/11624298/how-to-use-openfiledialog-to-select-a-folder
			using (var fbd = new FolderBrowserDialog())
			{
				DialogResult result = fbd.ShowDialog();

				if (result != DialogResult.OK)
					return;
				if (fbd.SelectedPath == null)
					return;
				if (fbd.SelectedPath == "")
					return;

				string[] files = Directory.GetFiles(fbd.SelectedPath, "*.dwg", SearchOption.AllDirectories);

				foreach (string dwgFileName in files)
					try
					{
						Editor ed = HostMgd.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
						ed.WriteMessage("ЗАМЕНА ШРИФТА в " + dwgFileName);

						ChangeFonts(dwgFileName);
					}
					catch (System.Exception e)
					{
						Document doc = HostMgd.ApplicationServices.Application.DocumentManager.MdiActiveDocument;

						doc.Editor.WriteMessage("Не удалось сконвертировать " + dwgFileName);
						doc.Editor.WriteMessage(e.Message);
					}
			}
		}

		private static void ChangeFonts(string fileName)
		{
			// https://csharp.hotexamples.com/examples/-/Database/ReadDwgFile/php-database-readdwgfile-method-examples.html
			using (Database db = new Database(false, true))
			{
				db.ReadDwgFile(fileName, FileShare.ReadWrite, true, "");

				//Document doc = HostMgd.ApplicationServices.Application.DocumentManager.Open(fileName);
				//Database db = doc.Database;

				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					using (TextStyleTable textStyleTable = tr.GetObject(db.TextStyleTableId, OpenMode.ForWrite) as TextStyleTable)
						foreach (ObjectId textstyleId in textStyleTable)
						{
							using (TextStyleTableRecord tstr = tr.GetObject(textstyleId, OpenMode.ForWrite) as TextStyleTableRecord)
							{

								if (tstr.FileName.ToLower().Contains("shx"))
								{
									FontDescriptor fd = tstr.Font;

									FontDescriptor newFont = new FontDescriptor("GOST 2.304 type A", fd.Bold, fd.Italic, fd.CharacterSet, fd.PitchAndFamily);
									double scale = 1;
									if (tstr.FileName == "CS_Gost2304.shx")
										scale = 1.25;

									//tstr = new TextStyleTableRecord()
									//{
									//	Font = newFont,
									//	IsShapeFile = false,
									//	XScale = scale
									//};

									tstr.FileName = "GOST 2.304 type A.ttf";
									tstr.Font = newFont;
									tstr.XScale = scale;
									//tstr.IsShapeFile = false;

									if (tstr.Database != db)
										tr.AddNewlyCreatedDBObject(tstr as DBObject, true);
								}
							}
						}

					//doc.Editor.Regen();

					tr.Commit();
				}

				string newFileName = Path.ChangeExtension(fileName, ".modified");

				db.Save();
			}
			//db.SaveAs(newFileName, db.OriginalFileVersion);
			// doc.CloseAndSave(newFileName); - not Implemented
			//doc.CloseAndDiscard();
		}
	}
}
